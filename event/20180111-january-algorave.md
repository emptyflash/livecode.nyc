---
title: January Algorave
location: Secret Project Robot
date: 2018-01-11 8:00 PM
image: /image/2018-january-algorave.png
link: https://www.facebook.com/events/165317170884185
---

==Time for live coded tunes and visuals ==

Scorpio Mouse

2050

Sean Lee

Ramsey Nasser

Codie

blindelephants

Mister Bomb

==Tickets==

$10

==More about Algoraves==

[https://algorave.com/](https://algorave.com/)

==Code of Conduct==

[https://livecode.nyc/coc.html](https://livecode.nyc/coc.html)
