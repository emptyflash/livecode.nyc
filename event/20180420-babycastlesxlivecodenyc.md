---
title: Algorave Arcade Happy Hour
location: Performance Space New York
date: 2018-04-20 7:00 PM
link: https://www.facebook.com/events/798696543658765
image: /image/2018-arcade-happy-hour.png
---

Performance Space New York teams up with artist-run collectives Babycastles and LiveCodeNYC for this special East Village edition of an Algorave inside an Arcade.

We’ll be presenting an Algorave, a code based dance party with visuals and music programmed in real time, inside an arcade filled with games made by independent videogame artists.

Algorave is a global collective of artists whose live, code-based dance raves are created through real-time algorithm-aided processes. 

Using systems built for algorithmic music and visuals, such as IXI Lang, puredata, Max/MSP, SuperCollider, Extempore, Fluxus, TidalCycles, Gibber, Sonic Pi, FoxDot and Cyril, musicians are able to compose and work live, breaking down artificial barriers between the people creating the software algorithms, the people making the music, and the people on the dance floor.

Friday, April 20

7pm - 11pm

Free with RSVP

👾 GAMES
* STATIC by Dennis Carr
* JUST ONE BOSS by Ayla Myers
* JIGGLYZONE by sylvie & hubol
* SPHERICAL SMACKDOWN by Rachél Bazelais & David Wallin

🎶 FEATURING THE SOUNDS OF:
* Lil Data (LONDON)
* Sean & Michael Lee(NY)
* Scorpion Mouse(NY)
* 2050 & Jared Lynch(NY)
* Codie(NY)
* Melody Loveless(NY)


📽️ WITH VISUALS FROM:
* Olivia Jack(SF/BOGOTA)
* Sarah Groff-Palermo(NY)
* Ulysses(NY)
* nom de nom(NY/BOSTON)
* Michael Lee(NY)

❤️ Algorave Arcade is co-presented by Performance Space New York with an implementation grant from the Doris Duke Charitable Foundation for the Building Demand for the Performing Arts Program.
