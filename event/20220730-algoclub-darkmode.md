---
title: Darkmode at Algoclub
location: The Cell Theatre, 338 West 23rd Street, New York NY, 10011
date: 2022-07-30 9:30 PM
duration: 4:00
link: https://www.fluxfactory.org/event/algoclub-light-mode-dark-mode/
image: /image/2022-algoclub-darkmode.png
categories: [algorave, club, music, art, computers]
---

# Enable Darkmode
It's show night at the algoclub, become immersed in the feedback loop of an algorithmic dance club. Listen, dance, and absorb mind-bending music and visuals created on the spot by livecoding artists.

## Lineup

### Visuals
- schwaz (instagram: _schwaz_)
- emptyflash (instagram: emptyflash_)
- gwenprime (website: gwenpri.me)
- s4y (instagram: s4y.live)
- shellylynnx (tiktok: shellylynnx)
- Melody Loveless (instagram: melodycodes)

### Music
- starlybri (instagram: starlyartstudio)
- azhadsyed (instagram: azhadsyed)
- alsoknownasrox (instagram: alsoknowasrox)
- Luciform (instagram: the_lesser_key)
- diegodukao (instagram: diegodukao)
- mgs (instagram: msg.nyc)

