---
title: Lightmode at Algoclub
location: The Cell Theatre, 338 West 23rd Street, New York NY, 10011
date: 2022-07-30 11:00 AM
duration: 8:00
image: /image/2022-algoclub-lightmode.png
categories: [algorave, club, music, art, computers, workshops, learning]
---

# Enable Lightmode
Have you ever wanted to make electronic dance music? Or some psychedelic video art? Did you know it can be as elegant as writing words, like writing a poem?
Learn livecoding techniques and processes from our community's performers and experts. Get started with audio tools like Sonic Pi and TidalCycles. Or come learn how to create audio reactive visuals with tools like Hydra and GLSL.

## Workshops

Sound as Collage: Mixed Media Zine Making with Sabrina Sims (starlybri)

Intro to Digital Video Synthesis with Hydra with  Cameron Alexander (emptyflash)

Beatmaking w/ Sonic Pi with Roxanne Harris (alsoknownasrox)

Anyone Can Jam: Audience Participation using Wireless Motion Controllers with Peter Williams (lightandny)

Intro to Shaders for Programers with Char Stiles (charstiles)
