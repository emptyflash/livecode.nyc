---
title: October Algorave
location: H0lo
date: 2017-10-08 7:00 PM
image: /image/2017-october-algorave.png
link: https://www.facebook.com/events/1943884799198135
---

BASS. BEATS. BOOLEANS.

An evening of live coding music featuring Parrises Squares, Vinton Surf, Daniel Steffy and Reckoner.

Visuals by ¸¸.•*¨*• Joseph Gordon x ART404 •*¨*•.¸¸

Tickets at the door.

MORE ABOUT ALGORAVE.

[https://algorave.com/about/](https://algorave.com/about/)

SEE WHAT WE DO.

[https://vimeo.com/228411462](https://vimeo.com/228411462)
