---
title: Computer Pop Algorave
location: wonderville
date: 2021-10-08 8:00 PM
link: https://withfriends.co/event/12661166/computer_pop_livecodenyc_algorave
image: /image/10-8-2021_algorave_poster.png
---

### Music & Vis
Leftpop & Indira
Luisa Mei & Shader Park
Hank Shen & Edgardo
@jessie & s4y
Kengchakaj & Nitchafame
DOOM of the ROCK & Easterner
dan.dog & gwenpri.me
dj_dave & Char Stiles
