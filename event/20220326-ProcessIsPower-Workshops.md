---
title: Process is Power Workshops
location: wonderville
date: 2022-03-26 11:00 AM
link: https://www.bklynlibrary.org/calendar/algorave-livecode-nyc-central-library-info-20220326
image: /venueimage/2022-processispower-left.png
categories: [workshop, teaching, library, music, art, livecode, code]
---

### Description
Algorithm + Rave = Algorave.
Have you ever wanted to make music or amazing art?
Did you know it can be as easy as writing words, like writing a poem?

Join Livecode NYC and learn the basics of using text to create music. We have free workshops on how to get started with the popular languages Sonic Pi, Hydra, FoxDot, TidalCycles or SuperCollider. Or come learn how to visualize data and see what sound looks like. Space is limited, so search "Livecode NYC" on Eventbrite to get your free tickets now. Or attend all 6 online at the comfort of your computer remotely!
