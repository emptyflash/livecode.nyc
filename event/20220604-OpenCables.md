---
title: Open Cables
location: Caffeine Underground, 447 Central Ave Brooklyn, NY 11221
date: 2022-06-04 4:00 PM
duration: 3:00
image: /image/2022-OpenCables.png
categories: [open mic, music, art, computers]
---

# Open Cables!

Think code is cool? wanna do it on stage? livecodenyc presents an open-mic format for livecoded performances by new and old members alike.
There will be an open sign up day-of!

### Featured Performances by

- LightAndny
- Starly Bri
- Schwaz
- One.\_.y1
- Alex Wang


