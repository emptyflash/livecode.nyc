module.exports = {
  title: "LiveCode.NYC",
  discord: "https://discord.gg/fcrAKbg",
  mailingList: "https://groups.google.com/forum/#!forum/livecodenyc",
  social: [
    { url: "https://twitter.com/livecodenyc", title: "Twitter" },
    { url: "https://instagram.com/livecodenyc", title: "Instagram" },
    { url: "https://www.facebook.com/livecodenyc", title: "Facebook" },
    { url: "https://gitlab.com/livecodenyc", title: "GitLab" }
  ],
  navLinks: [
    { path: "/events", title: "Events" },
    { path: "/members", title: "Members" },
    { path: "/tools", title: "Tools" },
    { path: "/network", title: "Network" },
    { path: "/press", title: "Press" },
    { path: "/contact", title: "Contact" }
  ],
  footerLinks: [
    { path: "/how-to-edit", title: "How to Submit Edits" },
    { path: "/design", title: "Design Assets" },
    { path: "/coc", title: "Code of Conduct" }
  ],
  repository: "https://gitlab.com/livecodenyc/livecodenyc.gitlab.io"
};
