---
name: DanceDirt
link: https://github.com/sicchio/DanceDirt
image: https://pbs.twimg.com/media/DrRnhkLXQAAAxX5?format=jpg&name=large
---

TidalCycles for Images in VLC Player, used in choreographic work. Made by Tom Murphy, used by Kate Sicchio. DanceDirt is made by [Kate Sicchio](/member/kate.md).
